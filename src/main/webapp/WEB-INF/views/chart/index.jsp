<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>

<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow-x : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		//$(".date").val(year + "-" + month + "-" + day);
		$("#sDate").val(caldate(7));
		$("#eDate").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 10 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
	}
	
	var handle = 0;
	
	
	function setToday(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		
		$(".date").val(year + "-" + month + "-" + day);
		$(".time").val(hour + ":" + minute);
	};

	
	function upDateS(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		//changeDateVal();
	};
	
	function downDateS(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		//changeDateVal();
	};
	
	function upDateE(){
		var $date = $("#eDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#eDate").val(today);
		//changeDateVal();
	};
	
	function downDateE(){
		var $date = $("#eDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#eDate").val(today);
		//changeDateVal();
	};
	
	function getStartTime(){
		var url = "${ctxPath}/getStartTime.do";
		var param = "shopId=" + shopId;
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				startHour = data.split("-")[0]
				startMinute = data.split("-")[1]
				getDvcList();
			}, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});	
	};
	
	
	$(function(){
		getStartTime();
		
		$("#upS").click(upDateS);
		$("#downS").click(downDateS);
		$("#upE").click(upDateE);
		$("#downE").click(downDateE);
		
		if(sDate==""){
			sDate = window.localStorage.getItem("jig_sDate");
			eDate = window.localStorage.getItem("jig_eDate");
		};
		
		createNav("analysis_nav", 2);
		
		setEl();
		setDate();	
		time();
		
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		$("#search_table").css({
			"position": "absolute",
		    "top": getElSize(140),
		    "color": "white"
		});
		
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			//"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
			
		if(getParameterByName('lang')=='ko'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(100),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 9,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else if(getParameterByName('lang')=='en' || getParameterByName('lang')=='de'){
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(70),
				"position" : "absolute",
				"top" : marginHeight + getElSize(115),
				"z-index" : 9,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}else{
			$("#title").css({
				"color" : "white",
				"font-size" : getElSize(90),
				"position" : "absolute",
				"top" : marginHeight + getElSize(100),
				"z-index" : 9,
				"left" : marginWidth + getElSize(1265),
				"font-weight" : "700",
				"border-radius" : getElSize(10),
				"width" : getElSize(1320),
				"text-align" : "center",
				"vertical-align": "middle",
				"padding-bottom" : getElSize(0),
				"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black"
			})
		}
		
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 3,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").not(".dateController").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").not(".dateController").css({
			"padding" : getElSize(15),
		})
		
		
		$(".dateController").css({
			"width" : getElSize(70) *  1.2,
			"height" : getElSize(70),
		});
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
			"position" : "relative",
			"top" : getElSize(26)
		});
		
		$("#content_table td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#arrow_left").css({
			"width" : getElSize(60),
			"margin-right" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#arrow_right").css({
			"width" : getElSize(60),
			"margin-left" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#controller").css({
			"display" : "table",
			"background-color" : "black",
			"border-radius" : getElSize(70),
			"position" : "absolute",
			"bottom" : getElSize(600),
			"padding" : getElSize(15),
			//"width" : getElSize(600),
			"vertical-align" : "middle"
		});
		
		$("#controller").css({
			"left" : $("#content_table").offset().left - marginWidth + ($("#content_table").width()/2) - ($("#controller").width()/2)
		});
		
		$("img").css({
			"display" : "inline"
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(100),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#chart").css({
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
			"margin-top" : getElSize(20),
		});
		
		$("#upE").css({
			"margin-left" : getElSize(19)
		});
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
			"position" : "relative",
			"top" : getElSize(26)
		});
	};
	
	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	
	
	function getDvcList(){
		var url = "${ctxPath}/getJigList4Report.do";
		var param = "shopId=" + shopId;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log(data);
				json = data.dvcList;
				var tr = "";
				var options = "";
				options += "<option id='ALL'>${total}</option>"
				$(json).each(function(idx, data){
					options += "<option id=" + data.dvcId + " value='" + decodeURIComponent(data.name).replace(/\+/gi,' ') + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";
				});
				
				$("#dvcList").html(options);
				
			
				if(!first_load){
					$("#dvcList").val(dvc);
				};

				first_load = false;	
				$("#dvcList").val(dvc);
				
				
				if(dvc=="") {
					$("#dvcList").val($("#dvcList option:nth(0)").val());
					dvc = $("#dvcList option:nth(0)").html();
				}

				showWcDatabyDvc();
				/* $("#dvcList").change(function(){
					showWcDatabyDvc();
				}); */
				
			}
		});
	};

	var color;	
	var first_load = true;
	
	function showWcDatabyDvc(){
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		console.log($("#dvcList option:selected").attr("id"));
		
		var url = "${ctxPath}/getProductionCnt.do";
		var param = "dvcId=" + $("#dvcList option:selected").attr("id") + 
					"&sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&shopId=" + shopId;

		if($("#sDate").val() > $("#eDate").val()){
			alert("${cannot_start_date_more_recent}");
			return;
		}
		
		//console.log(param);
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				
				var json = data.wcList;
				
				if(json.length==0){
					$("#chart").hide();
// 					$("#chart").empty();
// 					$("#chart").css("height",0)
					$(".chartTable").remove();
					console.log("reload");
					table = "<div class='chartTable'>${recheck_mahcine_date}</div>"
					$("#tableContainer").append(table)
					
					$(".chartTable").css({
					    "background" : "black",
						"margin-left" : "15%",
						"margin-top" : "10%",
						"font-size" : "300%"
					})
					
					$("#controller").hide();
					return;
				}
				
				$("#chart").show();
				$("#controller").show();
				
				var start = new Date(sDate);
				var end = new Date(eDate);
				var n = (end - start)/(24 * 3600 * 1000)+1;

				var tr = "";

				wcList = new Array();
				var wc = new Array();
				
				wc.push("${machine}");
				wc.push("${plan}");
				wc.push("${performance}");
				
				wcList.push(wc);
				
				planBar = [];
				resultBar = [];
				
				tmpArray = [];
				dateList = [];
				
				cBarPoint = 0;
				cPage = 1;
				
				tmpPlanBar = 0;
				tmpResultBar = 0;
				tmpWcList = [];
				
				$(json).each(function(idx, data){
					dateList.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
					tmpArray = dateList;
					
					var wc = new Array();
					wc.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
					wc.push(data.tgCyl);
					wc.push(data.cntCyl);					
					
					wcList.push(wc);
					tmpWcList = wcList;
					
					var plan = Number(data.tgCyl);
					var result = Number(data.cntCyl);
					
					planBar.push(plan);
					resultBar.push(result);
					
					tmpPlanBar = planBar;
					tmpResultBar = resultBar;
				});
			
				var blank = maxBar - json.length;
				maxPage = json.length - maxBar; 
					
				/* for(var i = 0; i < blank; i++){
					dateList.push("");
					var wc = new Array();
					wc.push("____");
					wc.push("");
					wc.push("");
					
					wcList.push(wc);

					planBar.push(0);
					resultBar.push(0);
				}; */
				
				for(var i = 0; i < maxBar - json.length % 10; i++){
					dateList.push("");
					var wc = new Array();
					wc.push("______");
					wc.push("");
					wc.push("");
					
					wcList.push(wc);
					
					planBar.push(-1);
					resultBar.push(-1);
				};
				
				if(json.length > maxBar){
					$("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
				};
				
				// 엑셀 저장 excel
				excelData = "";
				excelWcList = [];
				excelWcList = wcList;

				var excelLength = excelWcList.length;

				for(var i=0; i<excelWcList.length; i++){
					
					if(excelWcList[i][0].indexOf('______') == 0){
						excelLength = i;
						console.log("______ 의 최초 위치 : " + excelLength);
						break;
					}	
				}

				for(var i = 0; i < excelWcList[0].length; i++){	
					
					for(var j = 0; j < excelLength; j++){
						
						if(j==0){//매 줄 첫 번 째 td
							excelData += excelWcList[j][i] + ",";
						
						}else if(j==0 || i==0){//일자 
							if(j < excelLength -1){
								excelData += excelWcList[j][i] + ",";
							} else{
								excelData += excelWcList[j][i] + "LINE";
							}	
							
						}else {
							var n;
							if(typeof(excelWcList[j][i])=="number"){
								n = Number(wcList[j][i]);
							}else{
								n = "";
							};
							
							if(j < excelLength -1){
								excelData += n + ",";
							}else{
								excelData += n + "LINE";
							}
						};
					};
				};
				
				reArrangeArray();
				
				$(".chartTable").remove();
				var table = "<table class='chartTable' style='width: 100%; text-align: center; color: white'>";
				
				for(var i = 0; i < wcList[0].length; i++){
					table += "<tr class='contentTr'>";
					
					var bgColor, color;
					if(i==1){
						bgColor = planColor;
						color = "black";
					}else if(i==2){
						bgColor = resultColor;
						color = "black";
					}else{
						bgColor = "#323232";
					}
					
					for(var j = 0; j < wcList.length; j++){
						if(j==0){//매 줄 첫 번 째 td
							table += "<td class='title_' style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
						}else if(j==0 || i==0){//장비 이름
							table += "<td class='title_' style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
						}else {
							var n;
							if(typeof(wcList[j][i])=="number"){
								n = Number(wcList[j][i]);
							}else{
								n = "";
							};
							table += "<td class='content'>" + n + "</td>";
							
						};
					};
					table += "</tr>";
				};
				
				table += "</table>";
				
				$("#tableContainer").append(table);
				
				$(".title_").css({
					"width" : getElSize(300),
					"padding" : getElSize(5),
					"font-size" : getElSize(40)
				});
				
				$(".content").css({
					"font-size" : getElSize(40),
					"width" : ($(".contentTr").width() - getElSize(300))/10
				})
				
				chart("chart");
				addSeries();
			}
		});
	};
	
	var xAxis = new Array();
	var barChart;
	
	function chart(id){
		
		$("#" + id).css({
			"margin-left" : getElSize(50),
			"margin-right" : 0,
			"width" : $("#tableContainer table").width() - $("#tableContainer table td:nth(0)").width()-getElSize(20) + getElSize(80),
			"top" : $("#content_table").offset().top + $("#content_table").height() + getElSize(50)
		})
		
		$('#' + id).highcharts({
	        chart: {
	            type: 'column',
	            backgroundColor : 'rgb(50, 50, 50)',
	            height :getElSize(1150),
	            width : getElSize(3180),
	            marginRight:0,
	            marginBottom : getElSize(250)
	        },
	        title: {
	            text:false
	        },
	        xAxis: {
	            categories: dateList,
	            labels : {
	            	style : {
	            		"color" : "white",
	            		"font-size" : getElSize(40)
	            	}
	            }
	        },
	        yAxis: {
	            min: 0,
	            minRange: 2,
	            title: {
	                text: "COUNT",
	                style:{
	                	color : "white"
	                }
	            },
	            labels : {
	            	style : {
	            		"color" : "white",
	            		"font-size" : getElSize(30)
	            	},
	            	enabled:true
	            },
	            //reversed:true 
	        },
	        tooltip: {
	        	formatter: function () {
	                return this.y;
	            }
	        },
	        plotOptions: {
	            column: {
	                //stacking: 'normal',
	                dataLabels: {
	                	allowOverlap: true,
	                	formatter: function () {
	                		return (this.y!=-1) ? this.y : ""
    	            	},
	                    enabled: true,
	                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black',
	                    style: {
	                    	fontSize : getElSize(40)
	                        //textShadow: '0 0 3px black',
	                    }
	                }
	            },
	            series : {
	            	groupPadding : 0.09,
	            	pointWidth: getElSize(120),
	            	dataLabels: {
	                  	align: 'center',
	                    enabled: true,
	                    inside: true
	                  },
	            }
	        },
	        credits : false,
	        exporting : false,
	        legend:{
	        	enabled : false
	        },
	        series: []
	    });
		
		barChart = $("#" + id).highcharts();
	}
	
	var planColor = "#0080FF";
	var resultColor = "#A3D800";
	
	function addSeries(){
		
		barChart.addSeries({
			color : planColor,
			data : planBar
		}, true);
		
		barChart.addSeries({
			color : resultColor,
			data : resultBar
		}, true);
		
	};
	
	
	var dvc = replaceHyphen("${dvcName}");

	var chartWidth;
	var maxBar = 10;
	var tmpArray = new Array();
	var tmpPlanBar = new Array();
	var tmpResultBar = new Array();
	var tmpWcList = new Array();
	
	function reArrangeArray(){
		
		dateList = new Array();
		
		planBar = new Array();
		resultBar = new Array();
		
		wcList = new Array();
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
			dateList[i-cBarPoint] = tmpArray[i];
			planBar[i-cBarPoint] = tmpPlanBar[i];
			resultBar[i-cBarPoint] = tmpResultBar[i];
		};
		
		/* for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
			wcList[i-cBarPoint] = tmpWcList[i];	
		}; */
		
		if(tmpWcList.length == 0){
			
			var wc = new Array();
			
			wc.push("${machine}");
			wc.push("${plan}");
			wc.push("${performance}");
			
			//wcList.push(wc);
			
			wcList[0] = wc;
						
			for(var i = 0; i < maxBar - length % 10; i++){
				
				wc = [];
				wc.push("______");
				wc.push("");
				wc.push("");
				
				wcList.push(wc);
			};
			
		} else{
			
			for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
				wcList[i-cBarPoint] = tmpWcList[i];	
								
				var wc = new Array();
				
				wc.push("${machine}");
				wc.push("${plan}");
				wc.push("${performance}");
												
				wcList[0] = wc;
			};		
		}
		
		$("#controller font").html(addZero(String(cPage)) + " / " + addZero(String(Math.ceil((maxPage + maxBar) / 10))));
	};

	var cPage = 1;
	
	function nextBarArray(){
		if(cBarPoint>=maxPage || maxPage<=0){
			alert("${end_of_chart}");
			return;
		};
		cBarPoint += maxBar;
		cPage++;
		
		reArrangeArray();
		
		$(".chartTable").remove();
		var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		
		for(var i = 0; i < wcList[0].length; i++){
			table += "<tr class='contentTr'>";
			
			var bgColor, color;
			if(i==1){
				bgColor = planColor;
				color = "black";
			}else if(i==2){
				bgColor = resultColor;
				color = "black";
			}else{
				bgColor = "#323232";
			}
			
			
			for(var j = 0; j < wcList.length; j++){
				if(j==0){
					table += "<td class='title_' style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
				}else if(j==0 || i==0){
					table += "<td class='title_' style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
				}else {
					var n;
					if(typeof(wcList[j][i])=="number"){
						n = Number(wcList[j][i]);
					}else{
						n = "";
					};
					table += "<td class='content'>" + n + "</td>";
				};
			};
			table += "</tr>";
		};
		
		table += "</table>";
		$("#tableContainer").append(table)
		
		$(".title_").css({
					"width" : getElSize(300),
					"padding" : getElSize(5),
					"font-size" : getElSize(40)
				});
				
		$(".content").css({
			"font-size" : getElSize(40),
			"width" : ($(".contentTr").width() - getElSize(300))/10
		})
				
		//setEl();
		chart("chart");
		addSeries();
	};

	function prevBarArray(){
		if(cBarPoint<=0){
			alert("${first_of_chart}");
			return;
		};
		cBarPoint -= maxBar;
		cPage--;
		reArrangeArray();
		
		$(".chartTable").remove();
		var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		
		for(var i = 0; i < wcList[0].length; i++){
			table += "<tr class='contentTr'>";
			
			var bgColor, color;
			if(i==1){
				bgColor = planColor;
				color = "black";
			}else if(i==2){
				bgColor = resultColor;
				color = "black";
			}else{
				bgColor = "#323232";
			}
			
			for(var j = 0; j < wcList.length; j++){
				if(j==0){
					table += "<td class='title_' style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(250) + "'>" + wcList[j][i] + "</td>";
				}else if(j==0 || i==0){
					table += "<td class='title_' style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\")'>" + wcList[j][i] + "</td>";
				}else {
					var n;
					if(typeof(wcList[j][i])=="number"){
						n = Number(wcList[j][i]);
					}else{
						n = "";
					};
					table += "<td class='content'>" + n + "</td>";
				};
			};
			table += "</tr>";
		};
		
		table += "</table>";
		$("#tableContainer").append(table)
		
		$(".title_").css({
					"width" : getElSize(300),
					"padding" : getElSize(5),
					"font-size" : getElSize(40)
				});
				
		$(".content").css({
			"font-size" : getElSize(40),
			"width" : ($(".contentTr").width() - getElSize(300))/10
		})
		
		//setEl();
		chart("chart");
		addSeries();
	};
	var maxPage;
	var cBarPoint = 0;
	var overBar = false;
	
	var excelData;
	function csvSend(){
		var id = this.id;
		var sDate, eDate;
		var csvOutput;
		
		sDate = $("#sDate").val();
		eDate = $("#sDate").val();
		csvOutput = excelData;
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit();
	};
	
</script>

</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
		<input type="hidden" name="title" value="Production_Chart">
	</form>
	
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	
	<div id="time"></div>
	<div id="title"><spring:message code="prdChart_title"></spring:message></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					 <img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none"> 
					<%-- <img alt="" src="${ctxPath }/images/aidoo_control_logo.svg" class='left' id="home" style="display: none"> --%>
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'style="display: none">
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/analysis_left.png" class='menu_left'  style="display: none">
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right' style="display: none;">
					<!-- <table id="search_table" style="width: 100%; position:absolute; display:none"> -->
					<div id="search_table"> 
						&nbsp;&nbsp;
						<spring:message code="machine_group"></spring:message>
						<select id="dvcList"></select>
						<spring:message code="op_period"  ></spring:message>
						<button id="upS" class='dateController'>▲︎</button><button id="downS" class='dateController'>▼</button>
						<input type="date" class="date" id="sDate"> ~ <button id="upE" class='dateController'>▲︎</button><button id="downE" class='dateController'>▼</button>
						<input type="date" class="date" id="eDate">
						<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="showWcDatabyDvc()" >
						<button onclick="csvSend()" id="excel"> EXCEL </button>
					</div>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left' style="display: none">
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<%-- <Tr>
							<td> 
								&nbsp;&nbsp;
								<spring:message code="device"></spring:message>
								<select id="dvcList"></select>
								<spring:message code="op_period"  ></spring:message>
								<button id="upS" class='dateController'>▲︎</button><button id="downS" class='dateController'>▼</button>
								<input type="date" class="date" id="sDate"> ~ <button id="upE" class='dateController'>▲︎</button><button id="downE" class='dateController'>▼</button>
								<input type="date" class="date" id="eDate">
								<img alt="" src="${ctxPath }/images/search.png" id="search" onclick="showWcDatabyDvc()">
							</td>
						</Tr> --%>		
						<tr>
							<td>
								<div >
									<div id="chart" style="width: 100%">
										
									</div>
									<div id="controller">
										<img alt="" src="${ctxPath }/images/left_en.png" id="arrow_left"  onclick="prevBarArray();" style="display:none">
										<font style="display: table-cell; vertical-align:middle">01 / 00</font>
										<img alt="" src="${ctxPath }/images/right_en.png" id="arrow_right" onclick="nextBarArray();"style="display:none">
									</div>
									<div id="tableContainer" style="width: 100%"></div>
								</div>									
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' style="display: none" >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  style="display: none">
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	